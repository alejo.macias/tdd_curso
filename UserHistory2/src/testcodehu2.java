import org.junit.Test;
import static org.junit.Assert.*;

public class testcodehu2 {
	@Test
	public void testPH(){
	 	codehu2 HU2 = new codehu2();	
		assertEquals("13.0",HU2.Clasificacion2(7,"10-20","Alta","Basico","Mtp","Activo")); 
		assertEquals("9.0",HU2.Clasificacion2(7,"10-20","Baja","Alcalino","Homeopatico","Excipiente")); 
		assertEquals("7.0",HU2.Clasificacion2(7,"10-20","Media","Basico","Homeopatico","Activo"));
		assertEquals("6.0",HU2.Clasificacion2(6,"10-20","Media","Basico","Homeopatico","Activo")); 
		assertEquals("33.0",HU2.Clasificacion2(6,"10-20","Alta","Alcalino","Homeopatico","Activo")); 
		assertEquals("6.0",HU2.Clasificacion2(6,"10-20","Baja","Basico","Mtp","Excipiente"));
		assertEquals("9.0",HU2.Clasificacion2(7,"10-20","Baja","Basico","Homeopatico","Excipiente")); 
		assertEquals("4.0",HU2.Clasificacion2(6,"10-20","Media","Alcalino","Mtp","Activo")); 
		assertEquals("20.0",HU2.Clasificacion2(7,"10-20","Alta","Basico","Homeopatico","Activo"));
		
	}
}
