import org.junit.Test;
import static org.junit.Assert.*;

public class testcodehu {
	@Test
	public void testCaseVerde(){
	 	codehu HU = new codehu();	
		assertEquals("Verde",HU.Clasificacion(0)); 
		assertEquals("Verde",HU.Clasificacion(3.8)); 
	}
	@Test
	public void testCaseAzul() {
	 	codehu HU = new codehu();	
		assertEquals("Azul",HU.Clasificacion(3.81)); 
		assertEquals("Azul",HU.Clasificacion(8.25)); 
	}
	@Test
	public void testCaseAmarillo() {
	 	codehu HU = new codehu();	
		assertEquals("Amarillo",HU.Clasificacion(8.26)); 
		assertEquals("Amarillo",HU.Clasificacion(15)); 
	}
	@Test
	public void testCaseRojo() {
	 	codehu HU = new codehu();	
		assertEquals("Rojo",HU.Clasificacion(16)); 
		assertEquals("Rojo",HU.Clasificacion(200)); 
	}
}
